﻿// See https://aka.ms/new-console-template for more information

//Пользователь вводит целочисленное число (оно может быть как положительным, так и отрицательным).
//Нужно проверить, что число корректно без использования любого стороннего преобразования строки в число
//т.е. нельзя использовать int.Parse(), int.TryParse(), Convert.ToInt() и т.д..
//Если нет, то вывести сообщение и завершить программу. Если корретно, то опять же без вызова
//сторонних методов и без конвертирования строки в число найти сумму цифр введенного числа.


using System;

namespace Laba5TheSecondTask
{
    class Program
    {
        static void Main()
        {   
            string a = Console.ReadLine();
            int it=0;
            for (int i=0;i<10;i++)
            {
                if (a.Contains(i.ToString()))
                {
                    it += i;
                }
                a =a.Replace(i.ToString(), string.Empty);
            }
            if ((a=="")|(a=="-"))
            {
                Console.WriteLine(it);
            }
            else
            {
                Console.WriteLine("The number is incorrect");
            }
        }
    }
}